const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const postSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    user: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

const Post = mongoose.model('posts', postSchema);

module.exports = Post;