var mongoose = require('mongoose');
var Schema = mongoose.Schema; //permite definir los campos almacenados en cada documento junto con sus requisitos de validación y valores predeterminados

var ventaSchema = Schema({

    fecha:Date,
    tipodePago:String,
    numeroAprobacion:String,
    pagado:Boolean,
});



/*    nombre:String,
    escuela:String,
    universidad:String
    });
    */
//. Definimos que lo vamos a usar en nuestra aplicación como un modulo
const Venta = mongoose.model('Venta', ventaSchema);
module.exports = Venta;