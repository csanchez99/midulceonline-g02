var Producto = require('../models/product')

function producto(req, res) {
    res.status(200).send({
        message: 'probando un producto'
    })
}



function listar(req, res) {
    var consulta = Producto.find({});


    consulta.exec(function (err, result) {
        if (err) {
            res.send(message, 'karen')
        } else {
            if (!result) {
                res.send({
                    message: "No hay registros"
                })
            } else {
                res.send({
                    result
                })
            }
        }
    })
}


function guardarProducto(req, res) {
    var nuevoProducto = new Producto(req.body);
    nuevoProducto.save((err, result) => {
        res.send({
            message: result
        });
    });
}




function buscarProducto(req, res) {
    var idProducto = req.params.id;
    Producto.findById(idProducto).exec((err, result) => {
        if (err) {
            res.status(500).send({
                message: 'Error al momento de ejecutar la solicitud'
            });
        } else {
            if (!result) {
                res.status(404).send({
                    message: 'El registro a buscar no se encuentra disponible'
                });

            } else {
                res.status(200).send({
                    result
                });
            }
        }
    })
}

function borrarProducto(req, res) {
    var idProducto = req.params.id;
    Producto.findByIdAndRemove(idProducto, function (err, result) {
        if (err) {
            res.send({
                message: "No se encontro el producto"
            });

        } else {
            res.json(result);
        }
    })
}


module.exports = {
    producto,
    guardarProducto,
    buscarProducto,
    listar,
    borrarProducto
}