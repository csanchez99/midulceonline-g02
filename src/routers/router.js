const {Router} = require('express');

var controllerProducto=require('../controllers/controllerProducto')
var controllerVenta=require('../controllers/controllerVenta')
var controllerCliente=require('../controllers/controllerCliente')

const router=Router();



//PRODUCTO
router.get('/producto/test',controllerProducto.producto);
router.get('/producto/listar', controllerProducto.listar);
router.post('/producto/crear',controllerProducto.guardarProducto);
router.get('/producto/listar/:id', controllerProducto.buscarProducto);
router.delete('/producto/borrar/:id', controllerProducto.borrarProducto)
//VENTA
router.get('/venta/test',controllerVenta.venta);


//CLIENTE
router.get('/cliente/test',controllerCliente.cliente);
router.post('/cliente/crear',controllerCliente.guardarCliente);
router.get('/cliente/listar' , controllerCliente.listarCliente);
//router.post('/cliente/login', controllerCliente.login);
//router.put('/cliente/update/:nick', controllerTokens.verifyToken,controllerCliente.updateUser);
//router.get('/cliente/valid/token' , controllerTokens.verifyToken, controllerCliente.validToken);

module.exports=router;

// pasos para commit

/*
git init
git status
git add . 
git status
git commit -m "cambio hecho"
git pushS

*/