var express = require('express');
var bodyParser = require('body-parser');
var app = express();
app.use(express.json()); //transforma los datos a json
app.use(require('./src/routers/router.js'));
var mongoose = require('./src/db/connection')
//var router = require('./routers/router');
//var app = express();

// Add middleware
app.use(express.urlencoded({
    extended: true
}));

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, XRequested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS');
    res.header('Allow', 'GET, POST, PUT, PATCH, DELETE, OPTIONS');
    next();
});

// Add routers

module.exports = app;

//segun la guia
/*var
    express require("express"),
    app
express(),
    bodyParser
require("body parser"),
    methodOverride
require("method override")
mongoose
require('mongoose')
app
use(express json())
app
use(express urlencoded({
    extended
    true
}))

//
Configurar cabeceras y cors
app
use req res, next
res
header ('Access Control Allow Origin
res
header ('Access Control Allow Headers Authorization X API KEY, Origin X
Requested With Content Type Accept Access Control Allow Request Method
res
header ('Access Control Allow Methods ',',' POST, OPTIONS, PUT, DELETE')
res
header Allow ',',' POST, OPTIONS, PUT, DELETE')
next
})
module
exports app
 */